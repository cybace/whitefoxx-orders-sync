import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <img className="logo" src="/images/whitefoxx_logo.png" />
            </div>
        );
    }
}

export default Header;
