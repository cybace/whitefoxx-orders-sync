var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var crypto = require('crypto');
var dotenv = require('dotenv').config();
var express = require('express');
var nonce = require('nonce');
var path = require('path');
var querystring = require('querystring');
var request = require('request');
var session = require('express-session');

import App from '../client/components/App';
import React from 'react';
import ReactDOMServer from 'react-dom/server';

var APP_API_KEY = process.env.APP_API_KEY;
var APP_API_SECRET_KEY = process.env.APP_API_SECRET_KEY;
var APP_URI = process.env.APP_URI;
var APP_REDIRECT_URI = process.env.APP_REDIRECT_URI;
var APP_SCOPE = process.env.APP_SCOPE;
var APP_SHOP = process.env.APP_SHOP;
var APP_ADMIN_URI = process.env.APP_ADMIN_URI;

var app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : true
}));
app.use(cookieParser());
app.use(express.static('public'));
app.use(session({
    secret : 'foobarbaz',
    saveUninitialized : true,
    resave : true
}));

app.listen(3000, function () {
    console.log('Listening on port 3000');
});

app.get('/', function (req, res) {
    if (installed(req)) {
        res.render('app', {
            apiKey: APP_API_KEY,
            shop: req.session.shop,
            app: ReactDOMServer.renderToString(<App />)
        });
    } else {
        res.redirect('/install');
    }
});

app.get('/install', function (req, res) {
    if (installed(req)) {
        res.render('app', {
            apiKey: APP_API_KEY,
            shop: req.session.shop,
            app: ReactDOMServer.renderToString(<App />)
        });
    } else {
        // Need a login here
        var installUri =
            'https://' + APP_SHOP +
            '/admin/oauth/authorize?client_id=' + APP_API_KEY +
            '&scope=' + APP_SCOPE +
            '&redirect_uri=' + APP_REDIRECT_URI;
        res.redirect(installUri);
    }
});

app.get('/installed', function (req, res) {
    var shop = req.query.shop;
    var hmac = req.query.hmac;
    var code = req.query.code;
    if (shop && hmac && code) {
        var map = Object.assign({}, req.query);
        delete map['signature'];
        delete map['hmac'];
        var message = querystring.stringify(map);
        var providedHmac = Buffer.from(hmac, 'utf-8');
        var generatedHash = Buffer.from(
            crypto
            .createHmac('sha256', APP_API_SECRET_KEY)
            .update(message)
            .digest('hex'),
            'utf-8'
        );
        var hashEquals = false;
        try {
            hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
        } catch (e) {
            hashEquals = false;
        };
        if (!hashEquals) {
            return res.status(400).send('HMAC validation failed');
        }
        var accessTokenReq = {
            url: 'https://' + APP_SHOP + '/admin/oauth/access_token',
            method: 'POST',
            json: {
                "client_id" : APP_API_KEY,
                "client_secret" : APP_API_SECRET_KEY,
                code
            }
        };
        request(accessTokenReq, function (err, accessTokenRes) {
            if (err) {
                res.send(err);
            } else {
                req.session.accessToken = accessTokenRes.body.access_token;
                initWebhook(req, 'order_transactions/create', 'newOrder');
                res.redirect(APP_ADMIN_URI);
            }
        });
    } else {
        res.status(400).send('Required parameters missing');
    }
});

app.post('/webhook/newOrder', function (req, res) {
    res.sendStatus(200);
    var orderId = req.body.order_id;
    console.log('RECEIVED AN ORDER WITH THE ID: ' + orderId);  // TODO
    getOrderData(req, orderId);
});

function installed(req) {
    return req.session.accessToken;
}

function initWebhook(req, topic, address) {
    address = APP_URI + '/webhook/' + address;
    var webhook = {
        url: 'https://' + req.query.shop + '/admin/webhooks.json',
        method: 'POST',
        headers: {
            'X-Shopify-Access-Token': req.session.accessToken
        },
        json: {
            "webhook": {
                "topic": topic,
                "address": address,
                "format": "json"
            }
        }
    };
    request(webhook, function (err, res, body) {
        if (err) {
            return console.log(err);
        }
        console.log(body);
    });
}

function getOrderData(req, orderId) {
    orderId = '415503679546';
    console.log('GETTING DATA FOR THE ORDER WITH THE ID: ' + orderId);  // TODO
    orderDataReq = {
        url: 'https://' + req.query.shop + '/admin/orders/' + orderId + '.json',
        method: 'GET',
        headers: {
            'X-Shopify-Access-Token': req.session.accessToken
        }
    };
    request(orderDataReq, function (err, res, body) {
        if (err) {
            return console.log(err);
        }
        console.log(body);
    });
}
