import React from 'react';

class Balance extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="balance-container">
                    <div className="col-W88-H88">
                        <button className="button-balance-alter button-balance-decrease"></button>
                    </div>
                    <div className="col-WMIN176-H88">
                        <h1 className="balance">£100,253</h1>
                    </div>
                    <div className="col-W88-H88">
                        <button className="button-balance-alter button-balance-increase"></button>
                    </div>
                    <div className="clear_both"></div>
                </div>
            </div>
        );
    }
}

export default Balance;
