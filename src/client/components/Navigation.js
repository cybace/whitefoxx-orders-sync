import React from 'react';
import ReactDOM from 'react-dom';

class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSticky: false
        }

        this.determineIfSticky = this.determineIfSticky.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.determineIfSticky);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.determineIfSticky);
    }

    render() {
        console.log('RENDERING');
        if (this.state.isSticky) {
            return (
                <div className="foo-sticky"></div>
            );
        } else {
            return (
                <div className="foo-not-sticky"></div>
            );
        }
    }

    determineIfSticky() {
        var bounds = ReactDOM.findDOMNode(this).getBoundingClientRect();
        if (bounds.top <= 0) {
            if (this.state.isSticky == false) {
                console.log('SETTING STATE TO STICKY');
                this.state.isSticky = true;
                this.forceUpdate();
            }
        } else {
            if (this.state.isSticky == true) {
                console.log('SETTING STATE TO NOT STICKY');
                this.state.isSticky = false;
                this.forceUpdate();
            }
        }
    }
}

export default Navigation;
