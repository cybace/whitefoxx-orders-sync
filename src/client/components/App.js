import React from 'react';

import Header from './Header';
import Balance from './Balance';
import Navigation from './Navigation';

class App extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <Balance />
            </div>
        );
    }
}

export default App;
